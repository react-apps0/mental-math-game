import React, { Component } from 'react';
import './App.css';
import Score from './Score'
import Equation from './Equation'


function getRandomNumber(limit) {
  return Math.floor(Math.random() * limit)
}

function makeQuestion() {
  const value1= getRandomNumber(100)
  const value2= getRandomNumber(100)
  const value3= getRandomNumber(100)
  const proposedAnswer= getRandomNumber(3)+value1+value2+value3
  return [value1, value2, value3, proposedAnswer]
}

class Game extends Component {
  constructor(props){
    super(props)
    const [value1, value2, value3, proposedAnswer] = makeQuestion()
    this.state = {
      value1: value1,
      value2: value2,
      value3: value3,
      proposedAnswer: proposedAnswer,
      numQuestions: 0,
      numCorrect: 0
    }
  }
  
  checkAnswer = (selectedAns) => {
    const {value1, value2, value3, proposedAnswer} = this.state
    if((value1 + value2 + value3 === proposedAnswer) === selectedAns) {
      this.setState((currentState) => ({
        numCorrect: currentState.numCorrect+1,
      }))    	
    } 
    this.setState((currentState) => ({
      numQuestions: currentState.numQuestions+1,
    }))
    this.updateEquation()
  }

  updateEquation = () => {
    const [value1, value2, value3, proposedAnswer] = makeQuestion()
    this.setState((currentState) => ({	
      value1: value1,
      value2: value2,
      value3: value3,
      proposedAnswer: proposedAnswer,
    }))
  }
  
  render() {
    return (
        <div className="game">
          <h2>Mental Math</h2>
          <Equation
      		value1={this.state.value1} 
  			value2={this.state.value2} 
  			value3={this.state.value3} 
  			proposedAnswer={this.state.proposedAnswer}
  		  />
          <button onClick={() => this.checkAnswer(true)}>True</button>
          <button onClick={() => this.checkAnswer(false)}>False</button>
          <Score numCorrect={this.state.numCorrect} numQuestions={this.state.numQuestions} />
      </div>
    );
  }
}

export default Game;
